const express = require("express");
const mongoose = require ("mongoose")
const app = express();
app.use(express.json());
const port = 4000;

mongoose.connect("mongodb+srv://rmontealegre:admin123@cluster0.cuby1ca.mongodb.net/B204-to-dos?retryWrites=true&w=majority",

{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connectionn Error"));

db.once("open", () =>console.log(`We're connected to the cloud database.`));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
	
});

const User = mongoose.model ("User", userSchema);


app.post("/signup",(req,res)=>{

	console.log(req.body);
	
	User.findOne({username: req.body.username}, (err, result) =>{
		if (result !== null && result.username == req.body.username) {
		return res.send("Duplicate User Found");
	}

	else{
		let newUser = new User({
			username : req.body.username,
			password : req.body.password

		});

		newUser.save((saveErr, savedUser) =>{
			
			if (saveErr){
				return console.error(saveErr)
			}

			else{
				return res.send("New User Created")
			}

		});
	}

	})

});
app.listen(port, () => console.log(`Server is running at port: ${port}`));