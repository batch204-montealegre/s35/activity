const express = require("express");
const mongoose = require ("mongoose")
const app = express();
app.use(express.json());
const port = 4000;

mongoose.connect("mongodb+srv://rmontealegre:admin123@cluster0.cuby1ca.mongodb.net/B204-to-dos?retryWrites=true&w=majority",

{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connectionn Error"));

db.once("open", () =>console.log(`We're connected to the cloud database.`));

const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		default: "pending"
	}
});

const Task = mongoose.model ("Task", taskSchema);


app.get("/hello", (req, res) =>{
	res.send("Hello World");
	});

app.post("/tasks",(req,res)=>{

	console.log(req.body);
	
	Task.findOne({name: req.body.name}, (err, result) =>{
		if (result !== null && result.name == req.body.name) {
		return res.send("Duplicate Task Found");
	}

	else{
		let newTask = new Task({
			name : req.body.name

		});

		newTask.save((saveErr, savedTask) =>{
			
			if (saveErr){
				return console.error(saveErr)
			}

			else{
				return res.status(201).send("New Task Created")
			}

		});
	}

	})

});

app.get("/tasks", (req, res) =>{
	Task.find({}, (err, result) => {

		if (err) {
			return console.log(err);
		} else{
			return res.status(200).json({
				data:result
			})

		}
	})
});


app.listen(port, () => console.log(`Server is running at port: ${port}`));